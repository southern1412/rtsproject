using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RenderInfo
{
    public EMoveType MoveType;
    public EDirection Direction;
    public EWeaponType WeaponType;
    public SpriteRenderer SpriteRenderer;

}

public class CharacterRenderer : MonoBehaviour
{
    Player player;
    //CAnimationHandler cAnimationHandler;
    [SerializeField] RenderInfo[] m_RenderInfos;
    public RenderInfo curRenderInfo;
    CAnimationHandler cAnimationHandler;


    private void Awake()
    {
        player = GetComponent<Player>();
    }
    private void Start()
    {
        curRenderInfo = m_RenderInfos[0];
    }
    private void LateUpdate()
    {
        EMoveType moveType = EMoveType.idle;
        EDirection direction = EDirection.down;
        EWeaponType weaponType = player.weaponType;
        bool isFlipX = false;

        //이동은 캔슬하고 공격 실행, 공격은 캔슬되지 않고 이후 이동 실행
        if (player.isAttack == false && player.inputKeys.Count > 0)    
        { 
            moveType = EMoveType.walk;           
        }

        if (player.isAttack == false && player.inputKeys.Count == 0)
        {
            moveType = EMoveType.idle;
        }

        if (player.isAttack == true)
        {
            moveType = EMoveType.attack;
        }
       

        //캐릭터 방향 정의
        if (player.lastInput == KeyCode.DownArrow)
        {
            direction = EDirection.down;
        }
        else if (player.lastInput == KeyCode.LeftArrow)
        {
            direction = EDirection.side;
            isFlipX = false;
        }
        else if (player.lastInput == KeyCode.RightArrow)
        {
            direction = EDirection.side;
            isFlipX = true;
        }
        else if(player.lastInput == KeyCode.UpArrow)
        {
            direction = EDirection.up;
        }

        //캐릭터 좌우 반전 
        if (curRenderInfo.Direction == EDirection.side)
        {
            curRenderInfo.SpriteRenderer.flipX = isFlipX;
        }

        //플레이어가 들고 있는 무기 가져오기
        if (weaponType != EWeaponType.none)
        {
            weaponType = player.weaponType;
        }

        //계속 m_RenderInfos 배열을 돌면서 맞는 게 몇 번짼지 체크
        if (player.isAttack == false)
        {
            for (int i = 0; i < m_RenderInfos.Length; i++)
            {
                if (m_RenderInfos[i].MoveType == moveType &&
                    m_RenderInfos[i].Direction == direction)
                {
                    ChangeSprite2(m_RenderInfos[i]);
                }
            }
        }
        else if (player.isAttack == true)
        {
            for (int i = 0; i < m_RenderInfos.Length; i++)
            {
                if (m_RenderInfos[i].MoveType == moveType &&
                    m_RenderInfos[i].Direction == direction &&
                    m_RenderInfos[i].WeaponType == weaponType)
                {
                    ChangeSprite2(m_RenderInfos[i]);
                    if (cAnimationHandler.isAnimPlayed == false)
                    {
                        player.isAttack = false;
                    }
                }
            }
        }
    }
   
    void ChangeSprite(RenderInfo newRenderInfo)
    {
        if (curRenderInfo == newRenderInfo)
            return;

        if (curRenderInfo != null)
        {
            curRenderInfo.SpriteRenderer.gameObject.SetActive(false);
        }

        curRenderInfo = newRenderInfo;

        if (curRenderInfo != null)
        {
            curRenderInfo.SpriteRenderer.gameObject.SetActive(true);
        }

    }

    void ChangeSprite2(RenderInfo newRenderInfo)
    {
        if (curRenderInfo != newRenderInfo)
        {
            curRenderInfo.SpriteRenderer.gameObject.SetActive(false);
            newRenderInfo.SpriteRenderer.gameObject.SetActive(true);
            curRenderInfo = newRenderInfo;
        }
        else
        {
            curRenderInfo.SpriteRenderer.gameObject.SetActive(true);
        }
    }
}
