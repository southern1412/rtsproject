using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UI;

public enum EMoveType : int
{
    idle = 1,
    walk,
    attack
}

public enum EDirection : int
{
    down = 1,
    side,
    up
}

public enum EWeaponType : int
{
    none = 1,
    bow,
    sword
}

public class Player : MonoBehaviour
{
    public Vector2 inputVec;
    public float speed = 3.0f;
    public bool isAttack;
    public KeyCode lastInput;
    public List<KeyCode> inputKeys = new List<KeyCode>();
    public EWeaponType weaponType;

    Rigidbody2D rigid;
    //CAnimationHandler cAnimationHandler;

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
    
    }
    void Update()
    {
        if (isAttack == false)
        {
            //키입력 받기
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                inputKeys.Add(KeyCode.LeftArrow);
            }
            else if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                inputKeys.Remove(KeyCode.LeftArrow);
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                inputKeys.Add(KeyCode.RightArrow);
            }
            else if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                inputKeys.Remove(KeyCode.RightArrow);
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                inputKeys.Add(KeyCode.UpArrow);
            }
            else if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                inputKeys.Remove(KeyCode.UpArrow);
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                inputKeys.Add(KeyCode.DownArrow);
            }
            else if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                inputKeys.Remove(KeyCode.DownArrow);
            }
        }


        //마지막 키입력을 벡터값으로 넣어주기
        if (inputKeys.Count > 0)
        {
            lastInput = inputKeys.Last();

            if (lastInput == KeyCode.LeftArrow)
            {
                inputVec.x = -1;
                inputVec.y = 0;
            }
            else if (lastInput == KeyCode.RightArrow)
            {
                inputVec.x = 1;
                inputVec.y = 0;
            }
            else if (lastInput == KeyCode.UpArrow)
            {
                inputVec.y = 1;
                inputVec.x = 0;
            }
            else if (lastInput == KeyCode.DownArrow)
            {
                inputVec.y = -1;
                inputVec.x = 0;
            }
        }
        else
        {
            inputVec = new Vector2(0, 0);
        }

        if (weaponType != EWeaponType.none)
        {
            Attack();
        }
    }

    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        Vector2 nextVec = inputVec.normalized * speed * Time.fixedDeltaTime;
        rigid.MovePosition(rigid.position + nextVec);
    }

    void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isAttack = true;
            inputVec = Vector2.zero;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Item")
        {
            if (collision.name == "Item_Sword")
            {
                weaponType = EWeaponType.sword;
            }
            
        }

        Destroy(collision.gameObject);
    }
}


