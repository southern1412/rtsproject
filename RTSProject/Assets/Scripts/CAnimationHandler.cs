using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAnimationHandler : MonoBehaviour
{
    public bool isAnimPlayed;
    Animator m_Animator;

    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

    void StartofAnimation()
    {
        isAnimPlayed = true;
        Debug.Log(isAnimPlayed);
    }

    void EndofAnimation()
    {
        isAnimPlayed = false;
        Debug.Log(isAnimPlayed);
    }
}
