using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class SamplePlayer : MonoBehaviour
{
    [SerializeField] GameConfig _gameConfig;
    [SerializeField] Rigidbody2D _rigidbody;
    [SerializeField] float _speed;

    List<KeyCode> _inputKeys = new List<KeyCode>();
    Vector2 _inputVector;
    KeyCode _lastInputKeyCode = KeyCode.None;

    public KeyCode GetLastInputKeyCode()
    {
        return _lastInputKeyCode;
    }

    public Vector2 GetInputVector()
    {
        return _inputVector;
    }

    private void Start()
    {
        _speed = _gameConfig.DefaultCharacterSpeed;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            AddInput(KeyCode.LeftArrow);
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            RemoveInput(KeyCode.LeftArrow);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            AddInput(KeyCode.RightArrow);
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            RemoveInput(KeyCode.RightArrow);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            AddInput(KeyCode.UpArrow);
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            RemoveInput(KeyCode.UpArrow);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            AddInput(KeyCode.DownArrow);
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            RemoveInput(KeyCode.DownArrow);
        }

        if (_inputKeys.Count > 0)
        {
            KeyCode lastInput = _inputKeys.Last();

            if (lastInput == KeyCode.LeftArrow)
            {
                _inputVector.x = -1;
                _inputVector.y = 0;
            }
            else if (lastInput == KeyCode.RightArrow)
            {
                _inputVector.x = 1;
                _inputVector.y = 0;
            }
            else if (lastInput == KeyCode.UpArrow)
            {
                _inputVector.y = 1;
                _inputVector.x = 0;
            }
            else if (lastInput == KeyCode.DownArrow)
            {
                _inputVector.y = -1;
                _inputVector.x = 0;
            }
        }
        else
        {
            _inputVector = Vector2.zero;
        }
    }

    private void FixedUpdate()
    {
        Vector2 nextVec = _inputVector.normalized * _speed * Time.fixedDeltaTime;
        _rigidbody.MovePosition(_rigidbody.position + nextVec);
    }

    void AddInput(KeyCode keyCode)
    {
        _inputKeys.Add(keyCode);
        _lastInputKeyCode = _inputKeys.Last();
    }

    void RemoveInput(KeyCode keyCode)
    {
        _inputKeys.Remove(keyCode);

        if(_inputKeys.Count > 0 && _lastInputKeyCode == keyCode)
        {
            _lastInputKeyCode = _inputKeys.Last();
        }
    }
}
