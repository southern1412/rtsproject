using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EAnimationCategory : int
{
    NormalIdle = 1,
    NoramlWalk,
    BowIdle,
    BowWalk,
}

public enum EAnimationDirection : int
{
    Up = 1,
    Down,
    Side,
}

[Serializable]
public class RenderInfoStructure
{
    public EAnimationCategory animationCategory;
    public EAnimationDirection animationDirection;
    public SpriteRenderer spriteRenderer;
}

public class PlayerRenderer : MonoBehaviour
{
    [SerializeField] RenderInfoStructure[] _renderInfos;

    RenderInfoStructure _currentRenderInfo;
    SamplePlayer _samplePlayer;

    private void Start()
    {
        _samplePlayer = GetComponent<SamplePlayer>();
    }

    private void LateUpdate()
    {
        bool isMovePlayer = false;
        EAnimationDirection direction = EAnimationDirection.Down;
        bool isFlip = false;

        if(_samplePlayer.GetInputVector() != Vector2.zero)
        {
            isMovePlayer = true;
        }

        KeyCode lastInputCode = _samplePlayer.GetLastInputKeyCode();
        if (lastInputCode == KeyCode.LeftArrow)
        {
            direction = EAnimationDirection.Side;
            isFlip = false;
        }
        else if(lastInputCode == KeyCode.RightArrow)
        {
            direction = EAnimationDirection.Side;
            isFlip = true;
        }
        else if(lastInputCode == KeyCode.UpArrow)
        {
            direction = EAnimationDirection.Up;
        }
        else if(lastInputCode == KeyCode.DownArrow)
        {
            direction = EAnimationDirection.Down;
        }
        else
        {
            direction = EAnimationDirection.Down;
        }

        EAnimationCategory animationCategory = EAnimationCategory.NormalIdle;
        if(isMovePlayer)
        {
            animationCategory = EAnimationCategory.NoramlWalk;
        }

        for (int i =0; i < _renderInfos.Length; ++i)
        {
            if (_renderInfos[i].animationDirection == direction &&
                _renderInfos[i].animationCategory == animationCategory)
            {
                ChangeRenderInfo(_renderInfos[i]);
            }
        }

        if(_currentRenderInfo != null)
        {
            _currentRenderInfo.spriteRenderer.flipX = isFlip;
        }
    }

    void ChangeRenderInfo(RenderInfoStructure renderInfo)
    {
        if (_currentRenderInfo == renderInfo)
            return;

        if(_currentRenderInfo != null)
        {
            _currentRenderInfo.spriteRenderer.gameObject.SetActive(false);
        }

        _currentRenderInfo = renderInfo;

        if(_currentRenderInfo != null)
        {
            _currentRenderInfo.spriteRenderer.gameObject.SetActive(true);
        }
    }
}
